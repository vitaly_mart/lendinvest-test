<?php

namespace App\Exceptions;

/**
 * Class InterestCalculationDateRangeException
 * @package App\Exceptions
 */
class InterestCalculationDateRangeException extends \Exception
{

}
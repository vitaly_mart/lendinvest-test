<?php

namespace App;

use App\Exceptions\InterestCalculationDateRangeException;
use DateTime;

/**
 * Class Investment
 * @package App
 */
class InterestCalculation
{
    /**
     * @var Loan
     */
    private $loan;

    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * @var DateTime
     */
    private $endDate;

    /**
     * InterestCalculation constructor.
     *
     * @param Loan $loan
     * @param DateTime $startDate
     * @param DateTime $endDate
     *
     * @throws InterestCalculationDateRangeException
     */
    public function __construct(Loan $loan, DateTime $startDate, DateTime $endDate)
    {
        $this->loan = $loan;
        $this->startDate = $startDate;
        $this->endDate = $endDate;

        if ($startDate->format('Y-m') !== $endDate->format('Y-m')
            || $startDate->format('d') != 1
            || $endDate->format('d') != (clone $endDate)
                ->modify('last day of this month')
                ->format('d')
        ) {
            throw new InterestCalculationDateRangeException;
        }
    }

    /**
     * Run interest calculations
     */
    public function run()
    {
        foreach ($this->loan->getTranches() as $tranche) {
            foreach ($tranche->getInvestments() as $investment) {
                $this->calculateEarnings($tranche, $investment);
            }
        }
    }

    /**
     * @param Tranche $tranche
     * @param Investment $investment
     */
    private function calculateEarnings(Tranche $tranche, Investment $investment)
    {
        $investor = $investment->getInvestor();

        if ($investment->getDate() > $this->endDate) {
            return;
        }
        $days_in_month = (int)$this->endDate->format('d');
        $days_ratio = $investment->getDate()->format('m') === $this->endDate->format('m')
            ? ($days_in_month - (int) $investment->getDate()->format('d') + 1) / $days_in_month
            : 1;
        $interest = $investment->getAmount() * ($tranche->getRate() / 100) * $days_ratio;

        $investor->setAmount($investor->getAmount() + $interest);
    }
}
<?php

namespace App;

/**
 * Class Investor
 * @package App
 */
class Investor
{
    use CheckPositiveInteger;

    /**
     * @var float
     */
    private $amount;

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }
}
<?php

namespace App;

use App\Exceptions\InvestmentAmountLimitException;
use App\Exceptions\InvestmentDateRangeException;
use App\Exceptions\TrancheLimitMinValueException;
use App\Exceptions\TrancheRateMinValueException;

/**
 * Class Tranche
 * @package App
 */
class Tranche
{
    use CheckPositiveInteger;

    /**
     * @var int
     */
    private $rate;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var Loan
     */
    private $loan;

    /**
     * @var Investment[]
     */
    private $investments = [];

    /**
     * Tranche constructor.
     *
     * @param int $rate
     * @param int $limit
     */
    public function __construct(int $rate, int $limit)
    {
        $this->rate = $rate;
        $this->limit = $limit;

        $this->checkInteger($this->rate, TrancheRateMinValueException::class, 1);
        $this->checkInteger($this->limit, TrancheLimitMinValueException::class, 1);
    }

    /**
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate;
    }

    /**
     * @param Loan $loan
     */
    public function setLoan(Loan $loan)
    {
        $this->loan = $loan;
    }

    /**
     * @return Investment[]
     */
    public function getInvestments(): array
    {
        return $this->investments;
    }

    /**
     * @param Investment $investment
     */
    public function addInvestment(Investment $investment)
    {
        $this->investments[] = $this->validateInvestment($investment);
    }

    /**
     * @param Investment $investment
     *
     * @return Investment
     * @throws InvestmentAmountLimitException
     * @throws InvestmentDateRangeException
     */
    private function validateInvestment(Investment $investment)
    {
        if ($investment->getDate() < $this->loan->getStartDate() || $investment->getDate() > $this->loan->getEndDate()) {
            throw new InvestmentDateRangeException();
        }

        $total_investments = array_sum(array_map(function (Investment $inv) {
            return $inv->getAmount();
        }, $this->investments));

        if ($investment->getAmount() + $total_investments > $this->limit) {
            throw new InvestmentAmountLimitException;
        }

        return $investment;
    }
}
<?php

namespace App;

use App\Exceptions\WrongLoanDatesOrderException;
use DateTime;

/**
 * Class Loan
 * @package App
 */
class Loan
{
    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * @var DateTime
     */
    private $endDate;

    /**
     * @var Tranche[]
     */
    private $tranches = [];

    /**
     * Loan constructor.
     *
     * @param DateTime $startDate
     * @param DateTime $endDate
     */
    public function __construct(DateTime $startDate, DateTime $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->checkDatesOrder();
    }

    /**
     * @throws WrongLoanDatesOrderException
     */
    private function checkDatesOrder()
    {
        if ($this->endDate <= $this->startDate) {
            throw new WrongLoanDatesOrderException();
        }
    }

    /**
     * @return DateTime
     */
    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    /**
     * @return DateTime
     */
    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * @param Tranche $tranche
     */
    public function addTranche(Tranche $tranche)
    {
        $tranche->setLoan($this);
        $this->tranches[] = $tranche;
    }

    /**
     * @return Tranche[]
     */
    public function getTranches(): array
    {
        return $this->tranches;
    }
}
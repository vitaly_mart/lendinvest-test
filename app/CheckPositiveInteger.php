<?php

namespace App;

/**
 * Trait CheckPositiveInteger
 * @package App
 */
trait CheckPositiveInteger
{
    public function checkInteger(int $value, string $exception, int $min = null, int $max = null)
    {
        $filter_options = [];

        if ($min !== null) {
            $filter_options['min_range'] = $min;
        }

        if ($max !== null) {
            $filter_options['max_range'] = $min;
        }

        if (filter_var($value, FILTER_VALIDATE_INT, ['options' => $filter_options]) === false) {
            throw new $exception;
        }
    }
}
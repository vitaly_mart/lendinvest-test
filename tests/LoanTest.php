<?php

namespace Tests;

use App\Exceptions\InvestmentAmountLimitException;
use App\InterestCalculation;
use App\Investment;
use App\Investor;
use App\Loan;
use App\Tranche;
use Jchook\AssertThrows\AssertThrows;
use PHPUnit\Framework\TestCase;

class LoanTest extends TestCase
{
    use AssertThrows;

    public function test_flow()
    {

        $loan = new Loan(new \DateTime('2015-10-01'), new \DateTime('2015-11-15'));

        $tranche_a = new Tranche(3, 1000);
        $loan->addTranche($tranche_a);

        $tranche_b = new Tranche(6, 1000);
        $loan->addTranche($tranche_b);

        $investor_1 = new Investor();
        $investor_1->setAmount(1000);

        $investor_2 = new Investor();
        $investor_2->setAmount(1000);

        $investor_3 = new Investor();
        $investor_3->setAmount(1000);

        $investor_4 = new Investor();
        $investor_4->setAmount(1000);

        $investment1 = new Investment($investor_1, 1000, new \DateTime('2015-10-03'));
        $tranche_a->addInvestment($investment1);

        $this->assertThrows(InvestmentAmountLimitException::class, function () use($tranche_a, $investor_2) {
            $investment2 = new Investment($investor_2, 1, new \DateTime('2015-10-04'));
            $tranche_a->addInvestment($investment2);
        });

        $investment3 = new Investment($investor_3, 500, new \DateTime('2015-10-10'));
        $tranche_b->addInvestment($investment3);

        $this->assertThrows(InvestmentAmountLimitException::class, function () use($tranche_b, $investor_4) {
            $investment4 = new Investment($investor_4, 1100, new \DateTime('2015-10-25'));
            $tranche_b->addInvestment($investment4);
        });

        $calculation = new InterestCalculation($loan, new \DateTime('2015-10-01'), new \DateTime('2015-10-31'));
        $calculation->run();

        $this->assertEquals(1028.06, round($investor_1->getAmount(), 2));
        $this->assertEquals(1021.29, round($investor_3->getAmount(), 2));
    }
}